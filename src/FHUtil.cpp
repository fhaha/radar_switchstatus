#include "FHUtil.h"
#include <ofMain.h>
#include <string.h>
#include <stdlib.h>
#ifdef TARGET_WIN32
#include <windows.h>
using namespace std;
//Converting a WChar string to a Ansi string
std::string WChar2Ansi(LPCWSTR pwszSrc)
{
	int nLen = WideCharToMultiByte(CP_ACP, 0, pwszSrc, -1, NULL, 0, NULL, NULL); 

	if (nLen<= 0) return std::string(""); 

	char* pszDst = new char[nLen];
	if (NULL == pszDst) return std::string(""); 

	WideCharToMultiByte(CP_ACP, 0, pwszSrc, -1, pszDst, nLen, NULL, NULL);
	pszDst[nLen -1] = 0; 

	std::string strTemp(pszDst);
	delete [] pszDst; 

	return strTemp;
} 

std::string UTF82Ansi(std::string src) 
{
	int nLen = MultiByteToWideChar(CP_UTF8, 0, src.c_str(), -1, NULL, 0); 

	if (nLen<= 0) return std::string(""); 

	wchar_t* pszTmp = new wchar_t[nLen];
	if (NULL == pszTmp) return std::string(""); 

	MultiByteToWideChar(CP_UTF8,0,src.c_str(),-1,pszTmp,nLen);

	nLen = WideCharToMultiByte(CP_ACP, 0, pszTmp, -1, NULL, 0, NULL, NULL);
	char * pszDst = new char[nLen];

	WideCharToMultiByte(CP_ACP,0,pszTmp,-1,pszDst,nLen,NULL,NULL);
	pszDst[nLen -1] = 0; 

	std::string strTemp(pszDst);
	delete [] pszDst; 
	delete [] pszTmp;

	return strTemp;
}

string ws2s(wstring& inputws){ return WChar2Ansi(inputws.c_str()); } 

//Converting a Ansi string to WChar string 

std::wstring Ansi2WChar(LPCSTR pszSrc, int nLen) 
{
	int nSize = MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)pszSrc, nLen, 0, 0);
	if(nSize <= 0) return NULL; 

	WCHAR *pwszDst = new WCHAR[nSize+1];
	if( NULL == pwszDst) return NULL; 

	MultiByteToWideChar(CP_UTF8, 0,(LPCSTR)pszSrc, nLen, pwszDst, nSize);
	pwszDst[nSize] = 0; 

	if( pwszDst[0] == 0xFEFF) // skip Oxfeff
	for(int i = 0; i < nSize; i ++) 
	pwszDst[i] = pwszDst[i+1]; 

	wstring wcharString(pwszDst);
	delete pwszDst; 

	return wcharString;
} 

std::wstring s2ws(const string& s){ return Ansi2WChar(s.c_str(),s.size());} 
std::string ws2s(const std::wstring& ws){ return WChar2Ansi(ws.c_str()); }
std::string ws2utf8(const std::wstring & ws){
	int nLen = WideCharToMultiByte(CP_UTF8, 0, ws.c_str(), -1, NULL, 0, NULL, NULL);
	char * pszDst = new char[nLen];

	WideCharToMultiByte(CP_UTF8,0,ws.c_str(),-1,pszDst,nLen,NULL,NULL);
	pszDst[nLen -1] = 0; 

	std::string strTemp(pszDst);
	delete [] pszDst; 

	return strTemp;
}
#else
std::string ws2s(const std::wstring& ws)
{
    std::string curLocale = setlocale(LC_ALL, NULL);        // curLocale = "C";
    setlocale(LC_ALL, "");
    const wchar_t* _Source = ws.c_str();
    size_t _Dsize = 2 * ws.size() + 1;
    char *_Dest = new char[_Dsize];
    memset(_Dest,0,_Dsize);
    wcstombs(_Dest,_Source,_Dsize);
    std::string result = _Dest;
    delete []_Dest;
    setlocale(LC_ALL, curLocale.c_str());
    return result;
}

std::wstring s2ws(const std::string& s)
{
	std::string oldloc = setlocale(LC_ALL, "" );
	
    const char* _Source = s.c_str();
    size_t _Dsize = s.size() + 1;
    wchar_t *_Dest = new wchar_t[_Dsize];
    wmemset(_Dest, 0, _Dsize);
    mbstowcs(_Dest,_Source,_Dsize);
    std::wstring result = _Dest;
    delete []_Dest;
    setlocale(LC_ALL, oldloc.c_str());
    return result;
}
#endif

