#include "FHEditBox.h"
#include "ofGraphics.h"

FHEditBox::FHEditBox(ofParameter<string> _label, float width, float height){
	isFocused=false;
	setup(_label,width,height);
}

FHEditBox::~FHEditBox(){
    label.removeListener(this,&FHEditBox::valueChanged);
	ofRemoveListener(ofEvents().keyPressed,this,&FHEditBox::ebkeyPressed,OF_EVENT_ORDER_AFTER_APP);
	ofRemoveListener(ofEvents().keyReleased,this,&FHEditBox::ebkeyReleased,OF_EVENT_ORDER_AFTER_APP);
	ofRemoveListener(ofEvents().mousePressed, this,&FHEditBox::mousePressed,OF_EVENT_ORDER_AFTER_APP);
}

bool FHEditBox::getVisible(void)
{
	return isVisible;
}

void FHEditBox::setVisible(bool vis)
{
	isVisible = vis;
}

FHEditBox* FHEditBox::setup(ofParameter<string> _label, float width, float height) {
	pTTF = NULL;
    label.makeReferenceTo(_label);
    b.width  = width;
    b.height = height;
    generateDraw();
    label.addListener(this,&FHEditBox::valueChanged);
	ofAddListener(ofEvents().keyPressed,this,&FHEditBox::ebkeyPressed,OF_EVENT_ORDER_AFTER_APP);
	ofAddListener(ofEvents().keyReleased,this,&FHEditBox::ebkeyReleased,OF_EVENT_ORDER_AFTER_APP);
	ofAddListener(ofEvents().mousePressed,this,&FHEditBox::mousePressed,OF_EVENT_ORDER_AFTER_APP);
	label.set("");
	isVisible=false;
    return this;
}

FHEditBox* FHEditBox::setup(string labelName, string _label, float width, float height) {
	pTTF = NULL;
    label.set(labelName,_label);
    return setup(label,width,height);
}

void FHEditBox::generateDraw(){
	bg.clear();

	bg.setFillColor(thisBackgroundColor);
	bg.setFilled(true);
	bg.rectangle(b);

    string name;
    if(!getName().empty()){
    	name = getName() + ": ";
    }
	if(!pTTF)
		textMesh = getTextMesh(name + (string)label, b.x + textPadding, b.y + b.height / 2 + 4);
	else{
		
	}
	
}

void FHEditBox::clearValue(void)
{
	label.set("");
	dispvalue="";
}

void FHEditBox::ebkeyPressed(ofKeyEventArgs & key)
{
	int keynum = key.key;
	//printf("press key %x code %x\n",key.key,key.keycode);
	if(!isFocused|| !isVisible)
		return;
	if(keynum > 0 && keynum < 255 && isprint(keynum) && label.get().length() < 18){
		label+=keynum;
		password = label.get();
		dispvalue += 'X';
	}
	switch(key.key){
	case 0xd:{
		string nowpsd = label.get();
		theEvent(this,nowpsd);
		
		nowpsd.clear();
		nowpsd = "";
		setFocus(false);
		break;
			 }
	case 8:{
		string nowpsd = label.get();
		if(nowpsd.length()<=0)
			break;
		nowpsd.pop_back();
		dispvalue.pop_back();
		label.set(nowpsd);
		break;
		   }
	}
}

void FHEditBox::ebkeyReleased(ofKeyEventArgs & key)
{
	//printf("release key %x code %x\n",key.key,key.keycode);
}

void FHEditBox::render() {
	ofColor c = ofGetStyle().color;
	int nowtime = ofGetElapsedTimef();
	std::string tdispvalue = dispvalue;
	if(!isVisible)
		return;
	bg.draw();

	ofBlendMode blendMode = ofGetStyle().blendingMode;
	if(blendMode!=OF_BLENDMODE_ALPHA){
		ofEnableAlphaBlending();
	}
	if(isFocused && nowtime%2==0)
	{
		tdispvalue+="_";
	}
    ofSetColor(textColor);
	if(pTTF){
		pTTF->drawString(wName,b.x + textPadding, b.y + b.height / 2 + 4);
		ofRectangle rect = pTTF->getStringBoundingBox(wName,b.x + textPadding, b.y + b.height / 2 + 4);
		pTTF->drawString(tdispvalue,rect.x + rect.width + 5,rect.y+rect.height-3);
	}
	else{
		bindFontTexture();
		textMesh.draw();
		unbindFontTexture();
	}

    ofSetColor(c);
	if(blendMode!=OF_BLENDMODE_ALPHA){
		ofEnableBlendMode(blendMode);
	}
}

ofAbstractParameter & FHEditBox::getParameter(){
	return label;
}

void FHEditBox::valueChanged(string & value){
	generateDraw();
}
