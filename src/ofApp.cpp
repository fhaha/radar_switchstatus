#include "ofApp.h"
#include <sstream>

#include "Poco/Delegate.h"
#include "Poco/MD5Engine.h"
#include "Poco/DigestStream.h"
#include "Poco/DigestEngine.h"
#include "FHUtil.h"
using Poco::DigestOutputStream;
using Poco::DigestEngine;
using Poco::MD5Engine;
using Poco::Delegate;
//#include <limits> 
extern int data[60];
void ofApp::setup(){
	//message = "loading mySettings.xml";
	savedScr= false;
	authorized=false;
	chgpwd = false;
	saveImg.allocate(ofGetWidth(),ofGetHeight(),OF_IMAGE_COLOR_ALPHA);
	//saveImg.setImageType(OF_IMAGE_COLOR_ALPHA);
	if( XML.load("mySettings.xml") ){
		//message = "mySettings.xml loaded!";	   
		std::stringstream idname;
		
		for(int k=1;k<8;k++){
			idname<<"/string[@id=1_"<<k<<"]";
			d1[k-1]=s2ws(XML.getValue<string>(idname.str(),"nothing"));
			idname.clear();
			idname.str("");
		}
		for(int k=1;k<30;k++){
			idname<<"/string[@id=2_"<<k<<"]";
			d2[k-1]=s2ws(XML.getValue<string>(idname.str(),"nothing"));
			idname.clear();
			idname.str("");
		}
	    for(int k=1;k<30;k++){
			idname<<"/string[@id=3_"<<k<<"]";
			d3[k-1]=s2ws(XML.getValue<string>(idname.str(),"nothing"));
			idname.clear();
			idname.str("");
		}
		for(int k=0;k<14;k++){
			idname<<"/string[@id=4_"<<k<<"]";
			d4[k]=s2ws(XML.getValue<string>(idname.str(),"nothing"));
			idname.clear();
			idname.str("");
		}
		for(int k=1;k<31;k++){
			idname<<"/string[@id=5_"<<k<<"]";
			d5[k-1]=s2ws(XML.getValue<string>(idname.str(),"nothing"));
			idname.clear();
			idname.str("");
		}
		for(int k=1;k<31;k++){
			idname<<"/string[@id=6_"<<k<<"]";
			d6[k-1]=s2ws(XML.getValue<string>(idname.str(),"nothing"));
			idname.clear();
			idname.str("");
		}
		d7[0]=s2ws(XML.getValue<string>("/string[@id=7_1]"));//����
		d7[1]=s2ws(XML.getValue<string>("/string[@id=7_2]"));//����
		d7[2]=s2ws(XML.getValue<string>("/string[@id=7_3]"));//��
		d7[3]=s2ws(XML.getValue<string>("/string[@id=7_4]"));//��
		st1=s2ws(XML.getValue<string>("/string[@id=st1]"));
		st2=s2ws(XML.getValue<string>("/string[@id=st2]"));
		st3=s2ws(XML.getValue<string>("/string[@id=st3]"));
		st4=s2ws(XML.getValue<string>("/string[@id=st4]"));
		configstr=s2ws(XML.getValue<string>("/string[@id=config]"));
		m_wsTBLabel=s2ws(XML.getValue<string>("/string[@id=chgpwd]"));
		inputPwd=s2ws(XML.getValue<string>("/string[@id=inputpwd]","password"));
		newPwd=s2ws(XML.getValue<string>("/string[@id=newpwd]","new"));
		oldPwd=s2ws(XML.getValue<string>("/string[@id=oldpwd]","old"));
		confirmPwd=s2ws(XML.getValue<string>("/string[@id=confirmpwd]","confirm"));
		m_wsOk=s2ws(XML.getValue<string>("/string[@id=ok]","ok"));
		m_wsCancel=s2ws(XML.getValue<string>("/string[@id=cancel]","cancel"));
	}
	
	title=s2ws(XML.getValue<string>("/string[@id=title"));//��
	TTF.loadFont("simhei.ttf", 24);
	string fontfilename = XML.getValue<string>("/fontfile","simfang.ttf");
	int fontsize = XML.getValue<int>("/fontsize",12);
	int smallfontsize = XML.getValue<int>("/smallfontsize",10);
	TF.loadFont(fontfilename, fontsize);
	sTF.loadFont(fontfilename,smallfontsize);
	ebPasswd.setup("Password","");
	ebPasswd.setVisible(true);
	ebOldPasswd.setup("OldPassword","");
	ebNewPasswd.setup("NewPassword","");
	ebConfirmPasswd.setup("ConformPassword","");
	{
		ofXml authXML;
		if(authXML.load("authcode.xml")){
			pwd = authXML.getValue<string>("/string[@id=authcode]","1e0476186ab613a74a496a7ef5eef9bd");

		}
		else
			pwd = "1e0476186ab613a74a496a7ef5eef9bd";
	}
	PTF.loadFont("STKAITI.TTF",14,true,true);
	btnPanel.setFont(&PTF);
	btnPanel.setName(configstr);
	btnPanel.setDockSide(DSRIGHT);

	btnPanel.addButton(btnChgPwd.setup(m_wsTBLabel,&PTF));
	btnOk.setup(m_wsOk,&PTF);
	btnCancel.setup(m_wsCancel,&PTF);
	ofAddListener(btnChgPwd.btnEvent,this,&ofApp::ChgPwd);
	ofAddListener(btnOk.btnEvent,this,&ofApp::ConfirmChgPwd);
	ofAddListener(btnCancel.btnEvent,this,&ofApp::CancelChgPwd);
	ebPasswd.setPosition(ofGetWidth() /2 ,ofGetHeight()/2);
	ebOldPasswd.setPosition(ofGetWidth() /2 ,ofGetHeight()/2-2*ebNewPasswd.getHeight());
	ebNewPasswd.setPosition(ofGetWidth() /2 ,ofGetHeight()/2);
	ebConfirmPasswd.setPosition(ofGetWidth() /2 ,ofGetHeight()/2+2*ebNewPasswd.getHeight());
	btnOk.move(ofGetWidth() /2+20,ofGetHeight()/2+5*ebNewPasswd.getHeight());
	btnCancel.move(ofGetWidth() /2 + btnOk.getWidth() * 2+40,ofGetHeight()/2+5*ebNewPasswd.getHeight());
	btnOk.setVisible(false);
	btnCancel.setVisible(false);
	ebPasswd.setFont(&TF);
	ebPasswd.setFocus(true);
	ebOldPasswd.setFont(&TF);
	ebNewPasswd.setFont(&TF);
	ebConfirmPasswd.setFont(&TF);
	ebPasswd.setWName(inputPwd);
	ebOldPasswd.setWName(oldPwd);
	ebConfirmPasswd.setWName(confirmPwd);
	ebNewPasswd.setWName(newPwd);
	ebPasswd.theEvent += Poco::delegate(this,&ofApp::onUserEvent);
	ebConfirmPasswd.theEvent += Poco::delegate(this,&ofApp::onUserConfirmEvent);
	ebOldPasswd.theEvent += Poco::delegate(this,&ofApp::onUserOldEvent);
	ebNewPasswd.theEvent += Poco::delegate(this,&ofApp::onUserNewEvent);
    thread1.setup();
	thread2.setup();
	thread3.setup();
	thread4.setup();
	thread1.startThread();
	thread2.startThread();
	thread3.startThread();
	thread4.startThread();
	
	HWND myWin =ofGetWindowPtr()->getWin32Window();
	SetWindowText(myWin,L"���״̬���");
	//::SetWindowLong(myWin,GWL_EXSTYLE,GetWindowLong(myWin,GWL_EXSTYLE)|WS_EX_LAYERED);   
	//SetLayeredWindowAttributes(myWin,0,240,LWA_ALPHA);  
	//LONG oldLong = GetWindowLong(myWin,GWL_STYLE);
	//oldLong &= ~ WS_CAPTION;
	//SetWindowLong(myWin,GWL_STYLE,oldLong);
#ifdef _DEBUG
	rYValue = 0.0f;
#else
	rYValue = 0.0f;
#endif
	for(int i=0;i<sizeof(data)/sizeof(int);i++){
		data[i]=1;
	}
#ifndef _DEBUG
	//cammovement.startThread();
#endif
	//ofBackground(255,255,255);
	ofBackground(0,0,0);
	ofSetFrameRate(25);
}

ofApp::~ofApp()
{
	ebPasswd.theEvent -= Poco::delegate(this,&ofApp::onUserEvent);
	ebOldPasswd.theEvent -= Poco::delegate(this,&ofApp::onUserOldEvent);
	ebNewPasswd.theEvent -= Poco::delegate(this,&ofApp::onUserNewEvent);
	ebConfirmPasswd.theEvent -= Poco::delegate(this,&ofApp::onUserConfirmEvent);
	ofRemoveListener(btnChgPwd.btnEvent,this,&ofApp::ChgPwd);
	ofRemoveListener(btnCancel.btnEvent,this,&ofApp::CancelChgPwd);
	ofRemoveListener(btnOk.btnEvent,this,&ofApp::ConfirmChgPwd);
}

void ofApp::ConfirmChgPwd(int & eventarg)
{
	std::string nothing="nothing";
	onUserConfirmEvent(NULL,nothing);
}

void ofApp::CancelChgPwd(int &eventarg)
{
	btnOk.setVisible(false);
	btnCancel.setVisible(false);
	ebConfirmPasswd.setVisible(false);
	ebNewPasswd.setVisible(false);
	ebOldPasswd.setVisible(false);
	chgpwd=false;
}

void ofApp::ChgPwd(int &eventarg)
{
	if(btnChgPwd.getVisible()){
		printf("start change password!\n");
		chgpwd=true;
		btnOk.setVisible(true);
		btnCancel.setVisible(true);
		
		ebConfirmPasswd.setVisible(true);
		ebNewPasswd.setVisible(true);
		ebOldPasswd.setVisible(true);
	}
}

//
//--------------------------------------------------------------
void ofApp::update(){
#if 0
	if(!authorized){
		btnPanel.setVisible(false);
	}
	else{
		btnPanel.setVisible(true);
	}
#else
	btnPanel.setVisible(authorized);
#endif
}
#define ITEMPERVOL	33
//--------------------------------------------------------------
void ofApp::draw(){
	//ofRect(0,0, 400,300);
	
	int width = ofGetWidth();
	int height = ofGetHeight();
#ifdef _DEBUG
	ofPushMatrix();
	ofTranslate(height/2,0);
	ofRotate(rYValue,0,1,0);
	ofTranslate(-height/2,0);
	ofTranslate(0,0,rYValue);
#endif
	if(chgpwd){
		ebOldPasswd.draw();
		ebNewPasswd.draw();
		ebConfirmPasswd.draw();
	}
	else if(!authorized){
		ebPasswd.draw();
	}
	else{
		if(savedScr){
			ofSetColor(255,255,255,255);
			saveImg.draw(0,0);
		}
		else{
			ofRectangle titleBox = TTF.getStringBoundingBox(title,0,0);
			ofPoint titleRect = titleBox.getBottomRight();

			ofSetColor(0,255,0,255-rYValue);
			TTF.drawString(title,(width- titleRect.x ) /2 ,1.3*height/ITEMPERVOL);
			for(int i =2;i<ITEMPERVOL;i++)
			{
				ofLine(width/6,i*height/ITEMPERVOL,width/2,i*height/ITEMPERVOL);		
			}
			for(int i =2;i<ITEMPERVOL;i++)
			{
				ofLine(width*2/3-50,i*height/ITEMPERVOL,width-50,i*height/ITEMPERVOL);		
			}
			ofLine(50,height*2/ITEMPERVOL,width/6,height*2/ITEMPERVOL);
			TF.drawString(st1,50,2.5*height/ITEMPERVOL+5);
			ofLine(50,height*3/ITEMPERVOL,width/6,height*3/ITEMPERVOL);
			TF.drawString(d1[0],50,4*height/ITEMPERVOL+5);
			ofLine(50,5*height/ITEMPERVOL,width/6,5*height/ITEMPERVOL);
			TF.drawString(d1[1],50,7*height/ITEMPERVOL+5);
			ofLine(50,9*height/ITEMPERVOL,width/6,9*height/ITEMPERVOL);
			TF.drawString(d1[2],50,12*height/ITEMPERVOL+5);
			ofLine(50,15*height/ITEMPERVOL,width/6,15*height/ITEMPERVOL);
			TF.drawString(d1[3],50,17*height/ITEMPERVOL+5);
			ofLine(50,19*height/ITEMPERVOL,width/6,19*height/ITEMPERVOL);
			TF.drawString(d1[4],50,20*height/ITEMPERVOL+5);
			ofLine(50,21*height/ITEMPERVOL,width/6,21*height/ITEMPERVOL);
			TF.drawString(d1[5],50,21.5*height/ITEMPERVOL+5);
			ofLine(50,22*height/ITEMPERVOL,width/6,22*height/ITEMPERVOL);
			TF.drawString(d1[6],50,27*height/ITEMPERVOL+5);
			ofLine(50,32*height/ITEMPERVOL,width/2,32*height/ITEMPERVOL);
			//TF.drawString(d1[7],50,32.5*height/ITEMPERVOL+5);
			//ofLine(50,33*height/ITEMPERVOL,width/2,33*height/ITEMPERVOL);
	
			ofLine(width/2,height*2/ITEMPERVOL,width*2/3-50,height*2/ITEMPERVOL);
			TF.drawString(st1,width/2,2.5*height/ITEMPERVOL+5);
			ofLine(width/2,height*3/ITEMPERVOL,width*2/3-50,height*3/ITEMPERVOL);
			TF.drawString(d4[0],width/2,5.5*height/ITEMPERVOL+5);
			TF.drawString(d4[1],width/2,6.5*height/ITEMPERVOL+5);
			ofLine(width/2,height*10/ITEMPERVOL,width*2/3-50,height*10/ITEMPERVOL);
			sTF.drawString(d4[2],width/2,10.5*height/ITEMPERVOL+5);
			ofLine(width/2,height*11/ITEMPERVOL,width*2/3-50,height*11/ITEMPERVOL);
			TF.drawString(d4[3],width/2,11.5*height/ITEMPERVOL+5);
			ofLine(width/2,height*12/ITEMPERVOL,width*2/3-50,height*12/ITEMPERVOL);
			TF.drawString(d4[4],width/2,12.5*height/ITEMPERVOL+5);
			ofLine(width/2,height*13/ITEMPERVOL,width*2/3-50,height*13/ITEMPERVOL);
			TF.drawString(d4[5],width/2,16.5*height/ITEMPERVOL+5);
			TF.drawString(d4[6],width/2,17.5*height/ITEMPERVOL+5);
			ofLine(width/2,height*20/ITEMPERVOL,width*2/3-50,height*20/ITEMPERVOL);
			sTF.drawString(d4[7],width/2,20.5*height/ITEMPERVOL+5);
			ofLine(width/2,height*21/ITEMPERVOL,width*2/3-50,height*21/ITEMPERVOL);
			TF.drawString(d4[8],width/2,21.5*height/ITEMPERVOL+5);
			//TF.drawString(d4[10],width/2,23.5*height/ITEMPERVOL+5);
			ofLine(width/2,height*22/ITEMPERVOL,width*2/3-50,height*22/ITEMPERVOL);
			TF.drawString(d4[9],width/2,22.5*height/ITEMPERVOL+5);
			ofLine(width/2,height*23/ITEMPERVOL,width*2/3-50,height*23/ITEMPERVOL);
			TF.drawString(d4[10],width/2,24*height/ITEMPERVOL+5);
			ofLine(width/2,height*25/ITEMPERVOL,width*2/3-50,height*25/ITEMPERVOL);
			TF.drawString(d4[11],width/2,26*height/ITEMPERVOL+5);
			ofLine(width/2,27*height/ITEMPERVOL,width-50,27*height/ITEMPERVOL);
			TF.drawString(d4[12],width/2,29*height/ITEMPERVOL+5);
			ofLine(width/2,31*height/ITEMPERVOL,width*2/3-50,31*height/ITEMPERVOL);
			TF.drawString(d4[13],width/2,31.5*height/ITEMPERVOL+5);
			ofLine(width/2,32*height/ITEMPERVOL,width*2/3-50,32*height/ITEMPERVOL);


			ofLine(50,height*2/ITEMPERVOL,50,height*(ITEMPERVOL-1)/ITEMPERVOL);
			ofLine(width/2,height*2/ITEMPERVOL,width/2,(ITEMPERVOL-1)*height/ITEMPERVOL);
			ofLine(width*9/20,height*2/ITEMPERVOL,width*9/20,height*(ITEMPERVOL-1)/ITEMPERVOL);
			ofLine(width/4+32,height*2/ITEMPERVOL,width/4+32,height*(ITEMPERVOL-1)/ITEMPERVOL);
			ofLine(width/6,height*2/ITEMPERVOL,width/6,height*(ITEMPERVOL-1)/ITEMPERVOL);

			ofLine(width*2/3-50,height*2/ITEMPERVOL,width*2/3-50,(ITEMPERVOL-1)*height/ITEMPERVOL);
			ofLine(width*3/4-20,height*2/ITEMPERVOL,width*3/4-20,(ITEMPERVOL-1)*height/ITEMPERVOL);
			ofLine(width*19/20-50,height*2/ITEMPERVOL,width*19/20-50,(ITEMPERVOL-1)*height/ITEMPERVOL);
			ofLine(width-50,height*2/ITEMPERVOL,width-50,(ITEMPERVOL-1)*height/ITEMPERVOL);

			//ofLine(width/2,27*height/ITEMPERVOL,width-50,27*height/ITEMPERVOL);
			TF.drawString(st2,width/6,3*height/ITEMPERVOL-5);
			for (int i = 0; i < 29; i++)
			{		
				TF.drawString(d2[i],width/6,(i+4)*height/ITEMPERVOL-5);
			}
			TF.drawString(st3,width/4+30,3*height/ITEMPERVOL-5);
			for (int i = 0; i < 29; i++)
			{		
				TF.drawString(d3[i],width/4+30,(i+4)*height/ITEMPERVOL-5);
			}
			TF.drawString(st2,width*2/3-50,3*height/ITEMPERVOL-5);
			for (int i = 0; i < 29; i++)
			{		
				TF.drawString(d5[i],width*2/3-50,(i+4)*height/ITEMPERVOL-5);
			}
			TF.drawString(st3,width*3/4-20,3*height/ITEMPERVOL-5);
			for (int i = 0; i < 29; i++)
			{		
				TF.drawString(d6[i],width*3/4-20,(i+4)*height/ITEMPERVOL-5);
			}
			TF.drawString(st4,width*9/20,3*height/ITEMPERVOL-5);
			TF.drawString(st4,width*19/20-50,3*height/ITEMPERVOL-5);
			if(width!=0 && height !=0){
				saveImg.grabScreen(0,0,width,height);
#ifdef _DEBUG
				saveImg.saveImage("baga.png");
#endif
			}
			savedScr = true;
		}
		for (int i = 0; i < 29; i++)
		{
		
			if (!data[i])
			{
				ofSetColor(255,250,250);
				TF.drawString(d7[0],width*9/20,(i+4)*height/ITEMPERVOL-5);
			}
			else
			{
				ofSetColor(255,0,0);
				TF.drawString(d7[1],width*9/20,(i+4)*height/ITEMPERVOL-5);	
			
			}
		
		}
	
		for (int i = 29; i < 58; i++)
		{
			if (!data[i])
			{
		    
			
				if(i!=44){
					ofSetColor(255,250,250);
					TF.drawString(d7[0],width*19/20-50,(i-25)*height/ITEMPERVOL-5);
				}
				else{
					ofSetColor(255,0,0);
					TF.drawString(d7[2],width*19/20-50,(i-25)*height/ITEMPERVOL-5);
				}
			}
			else
			{
				if(i!=44){
					ofSetColor(255,0,0);
					TF.drawString(d7[1],width*19/20-50,(i-25)*height/ITEMPERVOL-5);	
				}
				else{
					ofSetColor(255,250,250);
					TF.drawString(d7[3],width*19/20-50,(i-25)*height/ITEMPERVOL-5);
				}
			
			}
		}
	}
#ifdef _DEBUG
	ofPopMatrix();
#endif
}
/*
std::wstring Ansi2WChar(LPCSTR pszSrc, int nLen) 
{
	int nSize = MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)pszSrc, nLen, 0, 0);
	if(nSize <= 0) return NULL; 

	WCHAR *pwszDst = new WCHAR[nSize+1];
	if( NULL == pwszDst) return NULL; 

	MultiByteToWideChar(CP_UTF8, 0,(LPCSTR)pszSrc, nLen, pwszDst, nSize);
	pwszDst[nSize] = 0; 

	if( pwszDst[0] == 0xFEFF) // skip Oxfeff
	for(int i = 0; i < nSize; i ++) 
	pwszDst[i] = pwszDst[i+1]; 

	wstring wcharString(pwszDst);
	delete pwszDst; 

	return wcharString;
} 
*/
//std::wstring s2ws(const string& s){ return Ansi2WChar(s.c_str(),s.size());} 
//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	//switch(key){
	//case 6:
	//	ofToggleFullscreen();
	//	break;
	//case 'f':
	//	if(authorized){
	//		ofToggleFullscreen();
	//	}
	//	break;
	//}
}

void ofApp::onUserNewEvent(const void * pSender,string& arg)
{
	//ebNewPasswd.setFocus(false);
	//ebConfirmPasswd.setFocus(true);
}

void ofApp::onUserOldEvent(const void * pSender,string& arg)
{
	//ebOldPasswd.setFocus(false);
	//ebNewPasswd.setFocus(true);
}

void ofApp::onUserConfirmEvent(const void * pSender,string& arg)
{
	string oldpwd = ebOldPasswd.getValue();
	if(oldpwd.empty()){
		ebOldPasswd.clearValue();
		ebOldPasswd.setFocus(true);
		return;
	}
	MD5Engine md5;
	DigestOutputStream ostr(md5);
	ostr << "LD" ;
	ostr << oldpwd;
	ostr.flush();
	const DigestEngine::Digest & digest = md5.digest();
	std::string result = DigestEngine::digestToHex(digest);
	//printf("onUserEvent %s result %s \n",arg.c_str(),result.c_str());
	if(result == pwd){
		string newpwd = ebNewPasswd.getValue();
		string cfpwd = ebConfirmPasswd.getValue();
		if(newpwd.empty() || newpwd !=cfpwd){
			
			ebNewPasswd.clearValue();
			ebNewPasswd.setFocus(true);
			ebConfirmPasswd.clearValue();
			ebConfirmPasswd.setFocus(false);
			ofSystemAlertDialog("�����벻ƥ���������Ϊ�գ�");
		}
		else{
			MD5Engine nmd5;
			DigestOutputStream nostr(nmd5);
			nostr << "LD" ;
			nostr << newpwd;
			nostr.flush();
			const DigestEngine::Digest & ndigest = nmd5.digest();
			std::string nresult = DigestEngine::digestToHex(ndigest);
			ofXml newXML;
			if(newXML.load("authcode.xml")){
				newXML.clear();
				newXML.addChild("resource");
				newXML.setTo("resource");
				newXML.addChild("string");
				newXML.setValue("string",nresult);
				newXML.setTo("string");
				newXML.setAttribute("id","authcode");
				//newXML.setValue("/resource/string
				newXML.save("authcode.xml");
			}
			else{
				printf("load authcode.xml failed!");
				newXML.clear();
				newXML.addChild("resource");
				newXML.setTo("resource");
				newXML.addChild("string");
				newXML.setValue("string",nresult);
				newXML.setTo("string");
				newXML.setAttribute("id","authcode");
				//newXML.setValue("/resource/string
				newXML.save("authcode.xml");
			}
			pwd=nresult;
			ofSystemAlertDialog("�޸�����ɹ�");
		}
		ebOldPasswd.clearValue();
		ebNewPasswd.clearValue();
		ebConfirmPasswd.clearValue();
		int j=0;
		CancelChgPwd(j);
	}
	else{
		ebOldPasswd.clearValue();
		ebOldPasswd.setFocus(true);
		ofSystemAlertDialog("���������");
		return;
	}
	
}



void ofApp::onUserEvent(const void * pSender,string & arg)
{
	MD5Engine md5;
	DigestOutputStream ostr(md5);
	ostr << "LD" ;
	ostr << arg;
	ostr.flush();
	const DigestEngine::Digest & digest = md5.digest();
	std::string result = DigestEngine::digestToHex(digest);
	//printf("onUserEvent %s result %s \n",arg.c_str(),result.c_str());
	if(result == pwd){
		authorized = true;
		ebPasswd.setVisible(false);
	}

	
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	savedScr=false;
	saveImg.resize(w,h);
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
