#ifndef _XC2_COM_H
#define _XC2_COM_H
#pragma pack(push)
#pragma pack(1)
typedef struct _st_xc22jy{
	unsigned short head; // 5E5E			
	char EC1GFS:1;
	char EC1GHJS:1;
	char EC1GCJS:1;
	char EC40MXCSZ:1;
	char EC40MZJSZ:1;
	//int M30HSCJC:1;
	char M30CSCJC:1;
	char ZJHHJC:1;
	char YZBHH:1;
	char JMB:1;
	char CYBHHJC:1;
	unsigned char crc;
}xc22jy,*pxc22jy;
#pragma pack(pop)
#endif
