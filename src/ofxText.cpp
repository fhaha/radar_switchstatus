/*
 *  ofxSimpleSlider.cpp
 *  Created by Golan Levin on 2/24/12.
 *
 */

#include "ofxText.h"

//-----------------------------------------------------------------------------------------------------------------------
void ofxText::setup (){
	ofAddListener(ofEvents().draw, this, &ofxText::draw);
	ofAddListener(ofEvents().mouseMoved, this, &ofxText::mouseMoved);
	//ofAddListener(ofEvents().mousePressed, this, &ofxButton::mousePressed);
	//ofAddListener(ofEvents().mouseReleased, this, &ofxButton::mouseReleased);
	//ofAddListener(ofEvents().mouseDragged, this, &ofxButton::mouseDragged);
	//ofAddListener(ofEvents().touchDown, this, &ofxSimpleSlider::defaultTouchHandler);
	//ofAddListener(ofEvents().touchUp, this, &ofxSimpleSlider::defaultTouchHandler);
	//ofAddListener(ofEvents().touchMoved, this, &ofxSimpleSlider::defaultTouchHandler);
	//ofAddListener(ofEvents().touchCancelled, this, &ofxSimpleSlider::defaultTouchHandler);
}

void ofxText::resize(float inw,float inh)
{
	width = inw; 
	height = inh;
	box.set(x,y, width, height); 
}

void ofxText::enable()
{
	bEnabled = true;
}
void ofxText::disable()
{
	bEnabled = false;
}

bool ofxText::isEnabled()
{
	return bEnabled;
}

void ofxText::move(float inx,float iny)
{
	x = inx;
	y = iny;
	box.setX(x);
	box.setY(y);
}


//----------------------------------------------------
void ofxText::setLabelString (string str){
	labelString = str;
	float stringWidth = (labelString + mText).size();
	resize(stringWidth * 8+10,height);
}

void ofxText::setText(string text){
	mText = text;
	float stringWidth = (labelString + mText).size();
	resize(stringWidth * 8+10,height);
}

void ofxText::clear()
{
	setText("");
}

std::string ofxText::getText(void)
{
	return mText;
}

//----------------------------------------------------
void ofxText::draw(ofEventArgs& event){
	
	ofEnableAlphaBlending();
	ofDisableSmoothing();
	ofPushMatrix();
	ofTranslate(x,y,0);
	
	// Use different alphas if we're actively maniupulating me. 
	float sliderAlpha = (bHasFocus) ? 128:64;
	float spineAlpha  = (bHasFocus) ? 255:160;
	
	// draw box outline
	ofNoFill();
	ofSetLineWidth(1.0);
	ofSetColor(64,64,64, spineAlpha); 
	ofRectRounded(0,0, width,height,2); 
	
	ofSetColor(0,0,0, sliderAlpha);	
	float stringWidth = (labelString+mText).size();
	ofDrawBitmapString( labelString + mText, width / 2 - stringWidth*8 /2, height/2 + 4); 
		
	ofPopMatrix();
	ofSetLineWidth(1.0);
	ofDisableAlphaBlending();
}

//----------------------------------------------------
void ofxText::mouseMoved(ofMouseEventArgs& event){
	if(box.inside(event.x,event.y)){
		bHasFocus = true;
	}
	else{
		bHasFocus = false;
	}
}
void ofxText::mouseDragged(ofMouseEventArgs& event){
	
}
void ofxText::mousePressed(ofMouseEventArgs& event){
	
}
void ofxText::mouseReleased(ofMouseEventArgs& event){
	
}

void ofxText::defaultMouseHandler(ofMouseEventArgs & event){
	if(box.inside(event.x,event.y)){
		std::ostringstream ost;
		ost << "defaultMouseHandler:" << " x:" << event.x << " y:" << event.y; 
		ost << " button: " << event.button;
		ofLog(OF_LOG_ERROR,ost.str().c_str());
	}
}

void ofxText::defaultTouchHandler(ofTouchEventArgs & event){
	if(box.inside(event.x,event.y)){
		std::ostringstream ost;
		ost << "defaultTouchHandler:" << " x:" << event.x << " y:" << event.y; 
		ost << " type: " << event.type;
		ofLog(OF_LOG_ERROR,ost.str().c_str());
	}
}

		

