#pragma once
#include <string>
#include "ofMain.h"
#include "FHDockPanel.h"

class CFHWidget
{
public:
	CFHWidget(void);
	virtual ~CFHWidget(void);
	CFHDockPanel * m_pWM;
protected:
	void render();
	void generateDraw();
	
private:
	std::wstring m_wsLabel;
};

