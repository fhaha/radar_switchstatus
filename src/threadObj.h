#pragma once
#include "ofMain.h"
#include <stdio.h>
#include <iostream>
#include "gp_com.h"
#include "sifu_com.h"
#include "xc1_com.h"
#include "xc2_com.h"

class threadObj : public ofThread
{
public:
	threadObj(const char * comname,unsigned char head);
	~threadObj(void);
	
	bool readdata(int ava);
	virtual void nodata()=0;
	bool setup();
	virtual bool CRCjudge()=0;
	//unsigned char calcCRC();	
	virtual void handledata()=0;	
	unsigned char calcCRC(unsigned char * pData,int len);
	unsigned char myhead;
	ofSerial serial;
protected:
	void threadedFunction();
	ofXml XML;
	int baochang;
	unsigned char neicundata[1024];
	unsigned char * pda;
	int getlen;
	int tailindex;
	bool isOpened ;
	string mycomname;
	int nodatacount;
	int isDebug;
	
	ofFileLoggerChannel lof;
};

class threadObj1 : public threadObj//gp
{
public:
	threadObj1();
	~threadObj1(void){};
	bool CRCjudge();
	//unsigned char calcCRC();	
	void handledata();	
	void nodata();
};
class threadObj2 : public threadObj//sf
{
public:
	threadObj2();
	~threadObj2(void){};
	bool CRCjudge();
	//unsigned char calcCRC();	
	void handledata();	
	void nodata();
};
class threadObj3 : public threadObj//xc1
{
public:
	threadObj3();
	~threadObj3(void){};

	bool CRCjudge();
	//unsigned char calcCRC();	
	void handledata();	
	void nodata();
};
class threadObj4 : public threadObj//xc2
{
public:
	threadObj4();
	~threadObj4(void){};
	bool CRCjudge();
	//unsigned char calcCRC();	
	void handledata();
	void nodata();
};