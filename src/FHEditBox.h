#pragma once

#include "ofxBaseGui.h"
#include "ofParameter.h"
#include "ofxTrueTypeFontUC.h"
#include "Poco/BasicEvent.h"

using Poco::BasicEvent;

class FHEditBox: public ofxBaseGui {
public:
    FHEditBox(){}
    FHEditBox(ofParameter<string> _label, float width = defaultWidth, float height = defaultHeight);
    virtual ~FHEditBox();
	BasicEvent<string> theEvent;
    FHEditBox * setup(ofParameter<string> _label, float width = defaultWidth, float height = defaultHeight);
    FHEditBox * setup(string labelName, string label, float width = defaultWidth, float height = defaultHeight);
	void setFont(ofxTrueTypeFontUC * ttf){
		pTTF = ttf;
	}
	void setWName(wstring name)
	{
		wName = name;
	}
    // Abstract methods we must implement, but have no need for!
	virtual bool mouseMoved(ofMouseEventArgs & args){
		if(b.inside(ofPoint(args.x,args.y))){
			//FHAHA find
			//printf("[Label]mouseMoved\n");
			return true;
		}
		return false;}
    virtual bool mousePressed(ofMouseEventArgs & args){
		if(isVisible && b.inside(ofPoint(args.x,args.y))){
			//FHAHA find
			//printf("[Label]mousePressed\n");
			isFocused=true;
			return false;
		}
		else{
			isFocused=false;
		}
	return false;}
    virtual bool mouseDragged(ofMouseEventArgs & args){if(b.inside(ofPoint(args.x,args.y))){
			//FHAHA find
			//printf("[Label]mouseDragged\n");
			return true;
		}
	return false;}
    virtual bool mouseReleased(ofMouseEventArgs & args){
		if(b.inside(ofPoint(args.x,args.y))){
			//FHAHA find
			//printf("[Label]mouseReleased\n");
			return true;
		}
		return false;}

	virtual void saveTo(ofBaseSerializer& serializer){};
	virtual void loadFrom(ofBaseSerializer& serializer){};


	template<class ListenerClass, typename ListenerMethod>
	void addListener(ListenerClass * listener, ListenerMethod method){
		label.addListener(listener,method);
	}

	template<class ListenerClass, typename ListenerMethod>
	void removeListener(ListenerClass * listener, ListenerMethod method){
		label.removeListener(listener,method);
	}

	string getValue(void)
	{
		return label.get();
	}
    string operator=(string v) { label = v; return v; }
    operator const string & ()       { return label; }

    ofAbstractParameter & getParameter();
	void ebkeyPressed(ofKeyEventArgs & key);
	void ebkeyReleased(ofKeyEventArgs & key );
	void setFocus(bool foc){
		isFocused = foc;
	}
	bool getFocus(void){
		return isFocused;
	}
	void clearValue(void);
	void setVisible(bool vis);
	bool getVisible(void);
protected:
    void render();
    ofParameter<string> label;
	string password;
	string dispvalue;
	//ofParameter<wstring> wlabel;
	wstring wName;
    void generateDraw();
    void valueChanged(string & value);
    bool setValue(float mx, float my, bool bCheckBounds){return false;}
	bool isVisible;
    ofPath bg;
    ofVboMesh textMesh;
	ofxTrueTypeFontUC * pTTF;
	bool isFocused;
};
