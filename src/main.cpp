#include "ofMain.h"
#include "ofApp.h"
#include <Windows.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <WinError.h>

#define MAX_LOADSTRING 100

// 全局变量:
HINSTANCE hInst;								// 当前实例
TCHAR szWindowClass[MAX_LOADSTRING];			// 主窗口类名


HINSTANCE g_Instance;   // Handler of current instance
HHOOK     g_Hook;       // Handler of hook

BOOL SetHook();
BOOL UnSetHook();


// The hook function (will be called by other processes)
static LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam);

//////////////////////////////////////////////////////////////////////////
//odprintf -- debug function
void __cdecl odprintf(const char* fmt, ...)
{
	char buf[4096], *p = buf;
	va_list args;


	va_start(args, fmt);
	p += vsnprintf_s(p, sizeof(buf), _TRUNCATE, fmt, args);
	va_end(args);


	while ( p > buf  &&  isspace(p[-1]) )
		*--p = '\0';
	*p++ = '\r';
	*p++ = '\n';
	*p   = '\0';


	OutputDebugStringA(buf);	//output as ANSI string	//OutputDebugString
}

LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	KBDLLHOOKSTRUCT* pkh = (KBDLLHOOKSTRUCT*)lParam;


	//HC_ACTION: wParam 和lParam参数包含了键盘按键消息
	if (nCode == HC_ACTION) 
	{
		if (::GetAsyncKeyState('1') & 0x8000)
		{
			odprintf("111111111111111111");
		}
		//判断函数调用时指定虚拟键的状态
		//BOOL bCtrlKey =	::GetAsyncKeyState(VK_CONTROL) & 0x8000;
		BOOL bCtrlKey =	::GetAsyncKeyState(VK_CONTROL)>>((sizeof(SHORT) * 8) - 1);


#if 0	
		if ((pkh->vkCode == VK_ESCAPE && bCtrlKey) ||					// Ctrl+Esc 开始菜单
			(pkh->vkCode == VK_TAB    && pkh->flags & LLKHF_ALTDOWN) ||		// Alt+TAB 屏幕切换菜单
			(pkh->vkCode == VK_ESCAPE && pkh->flags & LLKHF_ALTDOWN) ||		// Alt+Esc 屏幕切换 
			(pkh->vkCode == VK_LWIN   || pkh->vkCode==VK_RWIN)				// 左右Windows键
			)	
		{		
			odprintf("ok...i'am come here!");		//这里会出现两次, 因为
			return 1; 
		}
#else
		if(pkh->vkCode == VK_F4){
			HWND myWin =ofGetWindowPtr()->getWin32Window();
			SetWindowPos(myWin,HWND_BOTTOM,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
			
			ShowWindow(myWin,SW_MINIMIZE);
		}
		else if(pkh->vkCode == VK_F5 ){
			HWND myWin =ofGetWindowPtr()->getWin32Window();
			ShowWindow(myWin,SW_RESTORE);
			SetWindowPos(myWin,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
		}
#endif

	}


	// Call next hook in chain
	return ::CallNextHookEx(g_Hook, nCode, wParam, lParam);
}


BOOL SetHook()
{
	if (g_Instance || g_Hook)		// Already hooked!
		return TRUE;


	g_Instance = (HINSTANCE)::GetModuleHandle(NULL);
	g_Hook     = ::SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)KeyboardProc, g_Instance, 0);
	if (!g_Hook)
	{
		odprintf("SetHook error, error code=%ld", ::GetLastError());	//error code
		return FALSE;
	}


	return TRUE;								// Hook has been created correctly
}


BOOL UnSetHook()
{
	if (g_Hook) {								// Check if hook handler is valid
		::UnhookWindowsHookEx(g_Hook);			// Unhook is done here
		g_Hook = NULL;							// Remove hook handler to avoid to use it again
	}


	return TRUE;								// Hook has been removed
}

bool SetAutoRun(bool bAutoRun)
{
	HKEY hKey;
	TCHAR strRegPath[] = L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";//找到系统的启动项
	if (bAutoRun)
	{ 
		if (RegOpenKeyEx(HKEY_CURRENT_USER, strRegPath, 0, KEY_ALL_ACCESS, &hKey) == ERROR_SUCCESS) //打开启动项     
		{
			TCHAR szModule[_MAX_PATH];
			TCHAR oldModule[MAX_PATH];
			DWORD oldType = 0;
			DWORD retLen=MAX_PATH;
			bool needSet = false;
			GetModuleFileNameW(NULL, szModule, _MAX_PATH);//得到本程序自身的全路径
			if(ERROR_SUCCESS==RegQueryValueEx(hKey,L"SwitchStatus",NULL,&oldType,(LPBYTE)oldModule,&retLen)){
#ifdef _DEBUG
				MessageBox(NULL,oldModule,L"Info",0);
#endif
				if(wcscmp(szModule,oldModule)!=0){
					needSet = true;
				}

			}
			else{
				needSet = true;
			}
			if(needSet)
				RegSetValueEx(hKey,L"SwitchStatus", 0, REG_SZ, (const BYTE*)(LPCSTR)szModule, wcslen(szModule)*2); //添加一个子Key,并设置值，"Client"是应用程序名字（不加后缀.exe）
			RegCloseKey(hKey); //关闭注册表
			return true;
		}
		else
		{  
			MessageBox(NULL,L"系统参数错误,不能随系统启动",L"错误",MB_ICONWARNING);   
		}
	}
	else
	{
		if (RegOpenKeyEx(HKEY_CURRENT_USER, strRegPath, 0, KEY_ALL_ACCESS, &hKey) == ERROR_SUCCESS)      
		{
			RegDeleteValue (hKey,L"SwitchStatus");   
			RegCloseKey(hKey);
			return true;
		}
	}
	return false;
}

#ifdef _DEBUG
//========================================================================
int main( ){ 
	//if(!SetAutoRun(true))
	//	return -1;
	//if(!SetHook())
	//	return -1;
	ofSetupOpenGL(1152,864, OF_WINDOW);			// <-------- setup the GL context

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp( new ofApp());

}

#else

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	HANDLE hMutex = NULL;
    TCHAR* lpszName = L"TestStatusMutex";

    hMutex = CreateMutexW(NULL,FALSE,lpszName);
    DWORD dwRet=GetLastError();

    if(hMutex)
    {
        if(ERROR_ALREADY_EXISTS == dwRet)
        {
            //some warnings here...
            CloseHandle(hMutex);

            return -1;
        }
    }
	if(!SetAutoRun(true))
		return -1;
	if(!SetHook())
		return -1;
	ofSetupOpenGL(1152,864, OF_WINDOW);			// <-------- setup the GL context

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp( new ofApp());
	UnSetHook();
	return 0;
}
#endif
