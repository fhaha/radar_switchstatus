#pragma once
#include "ofxTrueTypeFontUC.h"
#include "threadObj.h"
#include "camMovement.h"
#include "FHEditBox.h"
#include "FHDockPanel.h"
#include "FHButtonPanel.h"

//std::wstring s2ws(const std::string& s);
string do_fraction(long double value, int decplaces=3);
void floatToString( float a,string& res) ;
class ofApp : public ofBaseApp{

	public:
		~ofApp();

		threadObj1 thread1;
		threadObj2 thread2;
		threadObj3 thread3;
		threadObj4 thread4;

		void setup();
		void update();
		void draw();	
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		float rYValue ;

		void onUserEvent(const void * pSender,string & arg);
		void onUserNewEvent(const void * pSender,string & arg);
		void onUserOldEvent(const void * pSender,string & arg);
		void onUserConfirmEvent(const void * pSender,string & arg);
		void ChgPwd(int &eventarg);
		void ConfirmChgPwd(int & eventarg);
		void CancelChgPwd(int &eventarg);
	protected:
		CFHButtonPanel btnPanel;
		CFHButton btnChgPwd;
		CFHButton btnOk;
		CFHButton btnCancel;
		ofImage saveImg;
		bool authorized;
		bool chgpwd;
		bool savedScr;
		ofXml XML;
		FHEditBox ebPasswd;
		FHEditBox ebOldPasswd;
		FHEditBox ebNewPasswd;
		FHEditBox ebConfirmPasswd;
		string message;
		string pwd ;
		ofxTrueTypeFontUC TTF;
		ofxTrueTypeFontUC TF;
		ofxTrueTypeFontUC sTF;
		ofxTrueTypeFontUC PTF;
		std::wstring d1[8];
		std::wstring d2[30];
		std::wstring d3[30];
		std::wstring d4[14];
		std::wstring d5[30];
		std::wstring d6[30];
		std::wstring d7[4];
		std::wstring title;
		std::wstring st1;
		std::wstring st2;
		std::wstring st3;
		std::wstring st4;
		std::wstring configstr;
		std::wstring m_wsTBLabel;
		std::wstring inputPwd;
		std::wstring newPwd;
		std::wstring oldPwd;
		std::wstring m_wsOk;
		std::wstring m_wsCancel;
		std::wstring confirmPwd;
		camMovement cammovement;

};
