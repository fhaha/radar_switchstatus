#ifndef _GPCOM_H
#define _GPCOM_H

#pragma pack(push)
#pragma pack(1)
typedef struct _st_gp2jy{
	unsigned short phead; // 5c5c			
	char XGTGF2:1;
	char XGTFG5:1;
	char XSFZJ2:1;
	char XSFZJ3:1;
	char XSFZJ4:1;
	char XSFZJ5:1;
	char XPLY2:1;
	char XPLY3:1;
	char XPLY4:1;
	char XPLY5:1;
	char XPLY6:1;
	char XPLY7:1;
	char KaSFQD2:1;
	char KaSFQD3:1;
	char KaSFQD4:1;
	char KaSFQD5:1;
	char HMBY2:1;
	char HMBY3:1;
	char XXDY:1;
	char XHKZB:1;
	unsigned char crc;
}gp2jy,*pgp2jy;
#pragma pack(pop)

#endif
