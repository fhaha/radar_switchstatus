#ifndef _SIFU_COM_H
#define _SIFU_COM_H

#pragma pack(push)
#pragma pack(1)
enum ZFT {
	FZ=0,
	ZZ=1,
	TZ=2
};
typedef struct _st_ff2kz{
	unsigned short head;	//5F 5F
	int who:1;	//0->fw , 1->fy
	int byk:1;	//1 bk 0 yk
	unsigned int zfz_tz:2;	//
	int glkgztc:1;
	int gwzt:1;
	int qddygzjc:1;
	int csj1zt:1;
	int csj2zt:1;
	int dj1zt:1;
	int dj2zt:1;
	int sdtjd0:1;
	int sdtjd1:1;
	int sdtjd2:1;
	int dj1sn:1;
	int dj2sn:1;
	unsigned char  crc;
}ff2kz,*pff2kz;

typedef struct _st_sf2jy{
	unsigned short head; //55 55
	char fwxb:1;
	char fyxb:1;
	char txgzbm:1;
	char jmjsmkgz:1;
	char qdkzmkgz:1;
	char fydj1:1;
	char fydj2:1;
	char fycsj1:1;
	char fycsj2:1;
	char fwdj1:1;
	char fwdj2:1;
	char fwcsj1:1;
	char fwcsj2:1;
	char fwgw:1;
	char fwdygz:1;
	char fygw:1;
	char fydygz:1;
	char cc:1;
	unsigned char crc;
}st_sf2jy,*pst_sf2jy;
//sifu to antenna simulator
typedef struct _st_sf2as{
	unsigned short head;	//55 55
	float fyj;
	unsigned char ofyj[3];
	float fwj;
	unsigned char ofwj[3];
	char wm;
	unsigned char jlz[3];
	char fwbyk:1;
	char fybyk:1;
	char fwddjgz:1;
	char fyddjgz:1;
	unsigned char crc;
}st_sf2as,*pst_sf2as;

enum SFWM{
	SFIDLE=0,
	SFYS,		//YS 匀速 很少用到
	SFZX=3,	//ZX 指向 就是 搜索
	SFGZ=4	//GZ 跟踪	就是均匀
};

typedef struct _st_sfkz2ff{
	unsigned short head;	//
	float fwj;
	float fyj;
	float fwspeed;
	float fyspeed;
	char wm; 
	char txgz:1;
	char jmjsmkgz:1;
	char qdkzmkgz:1;
	char cc:1;
	char fwxb:1;
	char fyxb:1;
	char fwzfz:2;
	char fyzfz:2;
	unsigned char  crc;
}kz2ff,*pkz2ff;


#pragma pack(pop)

#endif
