#include "threadObj.h"

int data[58];

threadObj::threadObj(const char * comname,unsigned char head)
{
	pda = neicundata;
	myhead = head;
	mycomname = comname;
	tailindex=0;
	nodatacount =0;
	std::string logfilename = comname;
	logfilename+=".log";
	isDebug=0;
	lof.setFile(logfilename,true);
}

bool threadObj::setup()
{
	if( XML.load("comConfig.xml"))
	{
		string com = XML.getValue<string>(mycomname);
		isDebug = XML.getValue<int>("debug",0);
		lof.log(OF_LOG_VERBOSE,mycomname,"调试级别%d",isDebug);
		//serial.listDevices();
		int baud = 115200;
		if(!serial.setup(com,baud)){
			isOpened=false;
			//std::cout << "cannot open"<<com<<std::endl;
			lof.log(OF_LOG_VERBOSE,"Serial","串口%s打开失败",com.c_str());
			return false;
		}
		else{
			isOpened=true;
			//std::cout << "success open "<<com<<std::endl;
			lof.log(OF_LOG_VERBOSE,"Serial","串口%s打开成功",com.c_str());
		}
	}
	else
	{
		lof.log(OF_LOG_VERBOSE,"Serial","串口配置文件%s打开失败","comConfig.xml");
		return false;
	}
	return true;
}
threadObj::~threadObj(void)
{
}

unsigned char threadObj::calcCRC(unsigned char * pData,int len)
{
	int i = 0 ;
	unsigned char ret = 0;
	for(i=0;i<len-1;i++){
		ret += pData[i];
	}
	return ret;
};

//1 gp
//2 sfkz
//3 xc1
//4 xc2
threadObj1::threadObj1():threadObj("gp",0x5C)
{
	baochang = sizeof(gp2jy);
}
threadObj2::threadObj2():threadObj("sfkz",0x55)
{
	baochang = sizeof(st_sf2jy);
}
threadObj3::threadObj3():threadObj("xc1",0x5D)
{
	baochang=sizeof(xc12jy);
}
threadObj4::threadObj4():threadObj("xc2",0x5E)
{
	baochang=sizeof(xc22jy);
}
void threadObj :: threadedFunction()
{
	int ava = 0;
	cout << "thread " << mycomname << " started" << endl;
	if(!isOpened)
		return; 
	while(isThreadRunning()){
		ava =  serial.available();
		if(ava)
		{		
			if(isDebug>0)
				lof.log(OF_LOG_VERBOSE,mycomname,"有%d个数据待处理",ava);
			nodatacount=0;
			if(readdata(ava)){
			
				if (CRCjudge())
				{
					//cout << mycomname << "want handle data has buf len "<< tailindex << endl;
					if(isDebug>0)
						lof.log(OF_LOG_VERBOSE,"Serial","串口%s 处理数据缓冲区数据长度 %d",mycomname.c_str(),tailindex);
					handledata();
				}
				else{
					lof.log(OF_LOG_VERBOSE,mycomname,"CRC校验错误");
					//cout << mycomname << "crc error" << endl;
				}
				for (int i = 0; i < tailindex-baochang; i++)
				{
					pda[i]=pda[i+baochang];
				}
				tailindex=tailindex-baochang;
				
			}
		}		
		else
		{
			sleep(5);
			nodatacount++;
			if(nodatacount>500){
				nodata();
				nodatacount=0;
			}
		}

	}
}

bool threadObj::readdata(int ava)
{	
	if(isDebug>0)
		lof.log(OF_LOG_VERBOSE,mycomname,"读取前结尾在%d",tailindex);
	if(ava > sizeof(neicundata) - tailindex){
		ava = sizeof(neicundata) - tailindex;
	}
	if(isDebug>0)
		lof.log(OF_LOG_VERBOSE,mycomname,"需要读取 %d",ava);
	getlen = serial.readBytes(pda+tailindex,ava);
	//cout << mycomname << "read data len " << getlen << endl;
	if(isDebug>0)
		lof.log(OF_LOG_VERBOSE,mycomname,"读取了数据长度 %d，数据1 %x，数据2 %x 需要包长 %d",getlen,pda[0],pda[1],baochang);
	tailindex+=getlen;
	while(tailindex >=2){
		int i=0;
		if(pda[0]==myhead && pda[1]==myhead){
			if(tailindex >= baochang)
				return true;
		}
		else{
			for(i=1;i<tailindex;i++){
				pda[i-1]=pda[i];
			}
			tailindex--;
		}
	}
	return false;
}

bool threadObj1::CRCjudge()
{
	pgp2jy pPkg = (pgp2jy)pda;
	unsigned char getcrc = calcCRC((unsigned char *)pda,sizeof(gp2jy));
	return pPkg->crc == getcrc;
}
bool threadObj2::CRCjudge()
{
	pst_sf2jy pPkg = (pst_sf2jy)pda;
	unsigned char getcrc = calcCRC((unsigned char *)pda,sizeof(st_sf2jy));
	return pPkg->crc == getcrc;
}
bool threadObj3::CRCjudge()
{
	pxc12jy pPkg = (pxc12jy)pda;
	unsigned char getcrc = calcCRC((unsigned char *)pda,sizeof(xc12jy));
	return pPkg->crc == getcrc;
}
bool threadObj4::CRCjudge()
{
	pxc22jy pPkg = (pxc22jy)pda;
	unsigned char getcrc = calcCRC((unsigned char *)pda,sizeof(xc22jy));
	return pPkg->crc == getcrc;
}

void threadObj1::nodata()
{
	data[0] = 1;
	data[1] = 1;
	data[2] = 1;
	data[3] = 1;
	data[4] = 1;
	data[5] = 1;
	data[6] = 1;
	data[7] = 1;
	data[8] = 1;
	data[9] = 1;
	data[10] = 1;
	data[11] = 1;
	data[12] = 1;
	data[13] = 1;
	data[14] = 1;
	data[15] = 1;
	data[16] = 1;
	data[17] = 1;
	data[18] = 1;
	data[57] = 1;
}
void threadObj1::handledata()
{
	pgp2jy pPkg = (pgp2jy)pda;
	data[0] = pPkg->XGTGF2;
	data[1] = pPkg->XGTFG5;
	data[2] = pPkg->XSFZJ2;
	data[3] = pPkg->XSFZJ3;
	data[4] = pPkg->XSFZJ4;
	data[5] = pPkg->XSFZJ5;
	data[6] = pPkg->XPLY2;
	data[7] = pPkg->XPLY3;
	data[8] = pPkg->XPLY4;
	data[9] = pPkg->XPLY5;
	data[10] = pPkg->XPLY6;
	data[11] = pPkg->XPLY7;
	data[12] = pPkg->KaSFQD2;
	data[13] = pPkg->KaSFQD3;
	data[14] = pPkg->KaSFQD4;
	data[15] = pPkg->KaSFQD5;
	data[16] = pPkg->HMBY2;
	data[17] = pPkg->HMBY3;
	data[18] = pPkg->XHKZB;	
	data[57] = pPkg->XXDY;
	
}

void threadObj2::nodata()
{
	data[19] = 1;
	data[20] = 1;
	data[21] = 1;
	data[22] = 1;
	data[23] = 1;
	data[24] = 1;
	data[25] = 1;
	data[26] = 1;
	data[27] = 1;
	data[28] = 1;
	//change - 3
	data[49] = 1;
	data[50] = 1;
	data[51] = 1;
	data[52] = 1;
	data[53] = 1;
	data[54] = 1;
	data[55] = 1;
	data[56] = 1;
}
void threadObj2::handledata()
{
	pst_sf2jy pPkg = (pst_sf2jy)pda;
	data[19] = pPkg->fydj1;
	data[20] = pPkg->fydj2;
	data[21] = pPkg->fycsj1;
	data[22] = pPkg->fycsj2;
	data[23] = pPkg->fwdj1;
	data[24] = pPkg->fwdj2;
	data[25] = pPkg->fwcsj1;
	data[26] = pPkg->fwcsj2;
	data[27] = pPkg->fwxb;
	data[28] = pPkg->fyxb;
	//change -3
	data[49] = pPkg->fwgw;
	data[50] = pPkg->fwdygz;
	data[51] = pPkg->fygw;
	data[52] = pPkg->fydygz;
	data[53] = pPkg->txgzbm;
	data[54] = pPkg->jmjsmkgz;
	data[55] = pPkg->qdkzmkgz;
	data[56] = pPkg->cc;

}
void threadObj3::nodata()
{
	//change -1
	data[29] = 1;
	data[30] = 1;
	data[31] = 1;
	data[32] = 1;
	data[33] = 1;
	data[34] = 1;
	data[35] = 1;
	data[36] = 1;
	data[37] = 1;
	//data[39] = 1; // del old jmb
	//change -2
	data[38] = 1;
}
void threadObj3::handledata()
{
	pxc12jy pPkg = (pxc12jy)pda;
	//change -1
	data[29] = pPkg->YC1GFSJC;
	data[30] = pPkg->YC1GHJS;
	data[31] = pPkg->YC1GCJS;
	data[32] = pPkg->YC40MXCSZ;
	data[33] = pPkg->YC40MZJSZ;
	data[34] = pPkg->M30HSCJC;
	data[35] = pPkg->M30CSCJC;
	data[36] = pPkg->ZJHHJC;
	data[37] = pPkg->YZBHH;
	//data[39] = pPkg->JMB;
	data[38] = pPkg->CYBHHJC;
	
}
void threadObj4::nodata()
{
	//change -2
	data[39] = 1;
	data[40] = 1;
	data[41] = 1;
	data[42] = 1;
	data[43] = 1;
	data[44] = 1;
	data[45] = 1;
	data[46] = 1;
	data[47] = 1;
	//data[50] = 1;
	//change - 3
	data[48] = 1;
}
void threadObj4::handledata()
{
	pxc22jy pPkg = (pxc22jy)pda;
	//change -2
	data[39] = pPkg->EC1GFS;
	data[40] = pPkg->EC1GHJS;
	data[41] = pPkg->EC1GCJS;
	data[42] = pPkg->EC40MXCSZ;
	data[43] = pPkg->EC40MZJSZ;
	data[44] = pPkg->JMB;//pPkg->M30HSCJC; change for NEW JMB
	data[45] = pPkg->M30CSCJC;
	data[46] = pPkg->ZJHHJC;
	data[47] = pPkg->YZBHH;
	//data[50] = pPkg->JMB;	//del
	//change -3
	data[48] = pPkg->CYBHHJC;
}