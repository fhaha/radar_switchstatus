/*
 *  ofxSimpleSlider.h
 *  Created by Golan Levin on 2/24/12.
 *
 */


#pragma once

#include "ofMain.h"

class ofxText {

	public:
		ofxText(){
			x = 0;
			y = 0; 
			width = 100; 
			height = 20;
			box.set(x,y, width, height); 
	
			bHasFocus = false;
			bEnabled = true;
			labelString = ""; 
			mText = "";
		}
		~ofxText(){

		}
		void	setup ();
		void	resize(float inw,float inh);
		void	move(float inx,float iny);
		void	enable();
		void	disable();
		bool	isEnabled();
		void	draw(ofEventArgs& event);
		void	mouseMoved(ofMouseEventArgs& event);
		void	mouseDragged(ofMouseEventArgs& event);
		void	mousePressed(ofMouseEventArgs& event);
		void	mouseReleased(ofMouseEventArgs& event);
		void	defaultMouseHandler(ofMouseEventArgs & event);
		void	defaultTouchHandler(ofTouchEventArgs & event);
	
		
		void	setLabelString (string str);
		void	setText(string text);
		void	clear();
		std::string getText(void);
		ofEvent<int> txtEvent;
		float getX(){return x;}
		float getY() { return y;}
		float getWidth() { return width;}
		float getHeight() { return height;}
	
	protected: 
		float	x;
		float	y; 
		float	width; 
		float	height;
		ofRectangle box; 
		bool	bHasFocus; 
		bool	bEnabled;
	
		string	labelString; 
		string	mText;
	
	
	
};
