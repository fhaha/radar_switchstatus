#ifndef _XCCOM_H
#define _XCCOM_H
#pragma pack(push)
#pragma pack(1)
typedef struct _st_xc12jy{
	unsigned short head; // 5D5D			
	char YC1GFSJC:1;
	char YC1GHJS:1;
	char YC1GCJS:1;
	char YC40MXCSZ:1;
	char YC40MZJSZ:1;
	char M30HSCJC:1;
	char M30CSCJC:1;
	char ZJHHJC:1;
	char YZBHH:1;
	//int JMB:1;
	char CYBHHJC:1;
	unsigned char crc;
}xc12jy,*pxc12jy;
#pragma pack(pop)
#endif
