#pragma once
#include "FHDockPanel.h"
class CFHButtonPanel :
	public CFHDockPanel
{
public:
	CFHButtonPanel(void);
	~CFHButtonPanel(void);
	virtual bool mousePressed(ofMouseEventArgs & args);
	//virtual bool mouseDragged(ofMouseEventArgs & args);
	virtual bool mouseReleased(ofMouseEventArgs & args);
	virtual bool mouseMoved(ofMouseEventArgs & args);
	virtual void generateDraw();
	virtual void render();
	void addButton(CFHButton & button);
	void setVisible(bool vis);
protected:

private:
	
	vector<CFHButton *> m_pButtons;
};

